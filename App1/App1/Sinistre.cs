﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class Sinistre
    {


        public Sinistre (int pid,int polissa,int peritoNo,int pinforme,DateTime? da,DateTime? dob,DateTime? dt,String pdes, String ptipus,String pEstat)
        {
            ID = pid;
            Polissa = polissa;
            PeritNo = peritoNo;
            Informe = pinforme;
            DataAssignacio = da;
            DataObertura = dob;
            DataTancament = dt;
            Desc = pdes;
            Tipus = ptipus;
            Estat = pEstat;

        }
        private int mID;

        public int ID
        {
            get { return mID; }
            set { mID = value; }
        }
        private int mPolissa;

        public int Polissa
        {
            get { return mPolissa; }
            set { mPolissa = value; }
        }


        private int mPeritNo;

        public int PeritNo
        {
            get { return mPeritNo; }
            set { mPeritNo = value; }
        }

        private int mInforme;

        public int Informe
        {
            get { return mInforme; }
            set { mInforme = value; }
        }
        private DateTime? mDataAssignacio;

        public DateTime? DataAssignacio
        {
            get { return mDataAssignacio; }
            set { mDataAssignacio = value; }
        }
        private DateTime? mDataObertura;

        public DateTime? DataObertura
        {
            get { return mDataObertura; }
            set { mDataObertura = value; }
        }
        private DateTime? mDataTancament;

        public DateTime? DataTancament
        {
            get { return mDataTancament; }
            set { mDataTancament = value; }
        }

        private String mDesc;

        public String Desc
        {
            get { return mDesc; }
            set { mDesc = value; }
        }
        private String mTipus;

        public String Tipus
        {
            get { return mTipus; }
            set { mTipus = value; }
        }
        private String mEstat;

        public String Estat
        {
            get { return mEstat; }
            set { mEstat = value; }
        }






    }
}
