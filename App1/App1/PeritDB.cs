﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class PeritDB
    {

        public static int ExisteixrPerit(int codi)
        {


            string server = "127.0.0.1";
            string database = "prueba";
            string user = "root";
            string pswd = "informatica";

            string connectionString = "Server = " + server + ";database = " + database + ";uid = " + user + ";password = " + pswd + ";" + "SslMode = None;" + "charset = utf8";
            System.Text.EncodingProvider ppp;
            ppp = System.Text.CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(ppp);
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    MySqlCommand getCommand = connection.CreateCommand();
                    getCommand.CommandText = "SELECT COUNT(*) FROM PERIT WHERE NUMERO = @Val1 ";
                    getCommand.Parameters.AddWithValue("@Val1", codi);
                    return Convert.ToInt32(getCommand.ExecuteScalar());
                }
            }
            catch (MySqlException)
            {
                return 0;
            }
        }


        public static Boolean ModificarPerit(Perit p)
        {

            
            string server = "127.0.0.1";
            string database = "prueba";
            string user = "root";
            string pswd = "informatica";
            
            string connectionString = "Server = " + server + ";database = " + database + ";uid = " + user + ";password = " + pswd + ";" + "SslMode = None;" + "charset = utf8";
            System.Text.EncodingProvider ppp;
            ppp = System.Text.CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(ppp);
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    MySqlCommand getCommand = connection.CreateCommand();
                    getCommand.CommandText = "UPDATE  PERIT SET COGNOM1 = @Val1,COGNOM2 = @Val2,NOM= @Val3,NIF = @Val4,LOGIN = @Val5,PASSWORDMD5=@Val6,DATANAIX= @Val7 WHERE NUMERO = @Val8 ";
                    getCommand.Parameters.AddWithValue("@Val1", p.Cognom1);
                    getCommand.Parameters.AddWithValue("@Val2", p.Cognom2);
                    getCommand.Parameters.AddWithValue("@Val3", p.Nom);
                    getCommand.Parameters.AddWithValue("@Val4", p.Nif);
                    getCommand.Parameters.AddWithValue("@Val5", p.Login);
                    getCommand.Parameters.AddWithValue("@Val6", p.Password);
                    string datamysql = p.DataNaixament.ToString("yyyy/MM/dd",
                                CultureInfo.InvariantCulture);
                    getCommand.Parameters.AddWithValue("@Val7", datamysql);
                    getCommand.Parameters.AddWithValue("@Val8", p.ID);
                    getCommand.ExecuteNonQuery();
                    return true;
                    
                }
            }
            catch (MySqlException)
            {
                return false;
            }

        } 
        public static Boolean InsertarPerit(Perit p)
        {
            string server = "127.0.0.1";
            string database = "prueba";
            string user = "root";
            string pswd = "informatica";

            string connectionString = "Server = " + server + ";database = " + database + ";uid = " + user + ";password = " + pswd + ";" + "SslMode = None;" + "charset = utf8";
            System.Text.EncodingProvider ppp;
            ppp = System.Text.CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(ppp);
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    MySqlCommand getCommand = connection.CreateCommand();

                    getCommand.CommandText = "INSERT INTO PERIT (COGNOM1,COGNOM2,NOM,NIF,LOGIN,PASSWORDMD5,DATANAIX) VALUES (@Val1,@Val2,@Val3,@Val4,@Val5,@Val6,@Val7) ";
                    getCommand.Parameters.AddWithValue("@Val1", p.Cognom1);
                    getCommand.Parameters.AddWithValue("@Val2", p.Cognom2);
                    getCommand.Parameters.AddWithValue("@Val3", p.Nom);
                    getCommand.Parameters.AddWithValue("@Val4", p.Nif);
                    getCommand.Parameters.AddWithValue("@Val5", p.Login);
                    getCommand.Parameters.AddWithValue("@Val6", p.Password);
                    string datamysql = p.DataNaixament.ToString("yyyy/MM/dd",
                                CultureInfo.InvariantCulture);
                    getCommand.Parameters.AddWithValue("@Val7", datamysql);
                    getCommand.ExecuteNonQuery();
                    return true;

                }

                }
            catch (MySqlException)
            {
                return false;
            }
        }

        public static Boolean EliminarPerit(int codi)
        {
            string server = "127.0.0.1";
            string database = "prueba";
            string user = "root";
            string pswd = "informatica";

            string connectionString = "Server = " + server + ";database = " + database + ";uid = " + user + ";password = " + pswd + ";" + "SslMode = None;" + "charset = utf8";
            System.Text.EncodingProvider ppp;
            ppp = System.Text.CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(ppp);
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    MySqlCommand getCommand = connection.CreateCommand();
                    
                    getCommand.CommandText = "SELECT * FROM SINISTRE WHERE PERIT_NO = @Val1 ";
                    getCommand.Parameters.AddWithValue("@Val1",codi);
                    int x =Convert.ToInt32(getCommand.ExecuteScalar());
                    if (x != 0)
                    {
                        return false;
                    }
                    else
                    {
                        MySqlCommand getCommand2 = connection.CreateCommand();
                        getCommand2.CommandText = "DELETE FROM PERIT WHERE NUMERO = @Val1 ";
                        getCommand2.Parameters.AddWithValue("@Val1", codi);
                        getCommand2.ExecuteNonQuery();
                        return true;
                    }


                   

                }
            }
            catch (MySqlException)
            {
                return false;
                // Handle it 
            }
        }
        public static Perit GetPerit(int codi)
        {
            
            string server = "127.0.0.1";
            string database = "prueba";
            string user = "root";
            string pswd = "informatica";
            
           
            Perit perito =null;
            string connectionString = "Server = " + server + ";database = " + database + ";uid = " + user + ";password = " + pswd + ";" + "SslMode = None;" + "charset = utf8";
            System.Text.EncodingProvider ppp;
            ppp = System.Text.CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(ppp);
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    MySqlCommand getCommand = connection.CreateCommand();
                    getCommand.CommandText = "SELECT * FROM PERIT where NUMERO = @Val1";
                    getCommand.Parameters.AddWithValue("@Val1", codi);
                    using (MySqlDataReader reader = getCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            perito = new Perit(reader.GetInt32("NUMERO"), reader.GetString("COGNOM1"), reader.GetString("COGNOM2"), reader.GetString("NOM"), reader.GetString("NIF"), reader.GetDateTime("DATANAIX"), reader.GetString("LOGIN"), reader.GetString("PASSWORDMD5"));
                            //  _todoViewModel._allToDos.Add(new Todo(reader.GetString("whatToDO")));
                        }
                    }
                }
            }
            catch (MySqlException)
            {
                // Handle it 
            }
            return perito;
        }


        public static  ObservableCollection<Perit>  GetPerits()
        {
            ObservableCollection<Perit> peritos = new ObservableCollection<Perit>();
             string server = "127.0.0.1";
            string database = "prueba";
            string user = "root";
            string pswd = "informatica";

            string connectionString = "Server = " + server + ";database = " + database + ";uid = " + user + ";password = " + pswd + ";" + "SslMode = None;" + "charset = utf8";
            System.Text.EncodingProvider ppp;
            ppp = System.Text.CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(ppp);
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    MySqlCommand getCommand = connection.CreateCommand();
                    getCommand.CommandText = "SELECT * FROM PERIT";
                    using (MySqlDataReader reader = getCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            peritos.Add(new Perit(reader.GetInt32("NUMERO"), reader.GetString("COGNOM1"), reader.GetString("COGNOM2"), reader.GetString("NOM"),reader.GetString("NIF"),reader.GetDateTime("DATANAIX"),reader.GetString("LOGIN"), reader.GetString("PASSWORDMD5")));
                          //  _todoViewModel._allToDos.Add(new Todo(reader.GetString("whatToDO")));
                        }
                    }
                }
            }
            catch (MySqlException)
            {
                // Handle it 
            }
            return peritos;
        }
    }
}
