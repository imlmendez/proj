﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class PolissaDB
    {
        public static Polissa GetPolissa(int codi)
        {

            string server = "127.0.0.1";
            string database = "prueba";
            string user = "root";
            string pswd = "informatica";


            Polissa pol = null;
            string connectionString = "Server = " + server + ";database = " + database + ";uid = " + user + ";password = " + pswd + ";" + "SslMode = None;" + "charset = utf8";
            System.Text.EncodingProvider ppp;
            ppp = System.Text.CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(ppp);
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    MySqlCommand getCommand = connection.CreateCommand();
                    getCommand.CommandText = "SELECT DATA_FI,ID_POLISSA,CLIENT_NUM,IMPORT,IMPORTCONTINGUT,IMPORTCONTINENT,TIPUSHABITATGE,DATA_INICI FROM POLISSA where ID_POLISSA = @Val1";
                    getCommand.Parameters.AddWithValue("@Val1", codi);
                    using (MySqlDataReader reader = getCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int id = reader.GetInt32("ID_POLISSA");
                            int cli = reader.GetInt32("CLIENT_NUM");
                            float import = reader.GetFloat("IMPORT");
                            float importcontingut = reader.GetFloat("IMPORTCONTINGUT");
                            float importcontinent = reader.GetFloat("IMPORTCONTINENT");
                            String tipus = reader.GetString("TIPUSHABITATGE");
                            DateTime DataInici = reader.GetDateTime("DATA_INICI");
                            DateTime? dataFi = null;
                            if (!reader.IsDBNull(0))
                            {
                                dataFi = reader.GetDateTime("DATA_FI");
                            }
                            pol = new Polissa(id,DataInici,dataFi,cli,import,importcontingut,importcontinent,tipus);
                            //  pol = new Polissa(reader.GetInt32("NUMERO"), reader.GetString("COGNOM1"), reader.GetString("COGNOM2"), reader.GetString("NOM"), reader.GetString("NIF"), reader.GetDateTime("DATANAIX"), reader.GetString("LOGIN"), reader.GetString("PASSWORDMD5"));
                            //  _todoViewModel._allToDos.Add(new Todo(reader.GetString("whatToDO")));
                        }
                    }
                }
            }
            catch (MySqlException)
            {
                // Handle it 
            }
            return pol;
        }
    }
}
