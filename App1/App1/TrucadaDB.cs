﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class TrucadaDB
    {
        public static ObservableCollection<Trucada> GetTrucades(int codi)
        {
            ObservableCollection<Trucada> Trucades = new ObservableCollection<Trucada>();
            string server = "127.0.0.1";
            string database = "prueba";
            string user = "root";
            string pswd = "informatica";

            string connectionString = "Server = " + server + ";database = " + database + ";uid = " + user + ";password = " + pswd + ";" + "SslMode = None;" + "charset = utf8";
            System.Text.EncodingProvider ppp;
            ppp = System.Text.CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(ppp);
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    MySqlCommand getCommand = connection.CreateCommand();
                    getCommand.CommandText = "SELECT * FROM TRUCADA WHERE SINISTRE_ID = @Val9";
                    getCommand.Parameters.AddWithValue("@Val9", codi);
                    using (MySqlDataReader reader = getCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {

                            int numtrucada = reader.GetInt32("NUM_TRUCADA");
                            DateTime hora = reader.GetDateTime("DATEHORA");
                            String desc = reader.GetString("DESCRIPCIOTRUCADA");
                            String persona = reader.GetString("PERSONACONTACTE");
                            Trucada trr = new Trucada(numtrucada,codi,hora,desc,persona);
                            Trucades.Add(trr);
                           // Sinistre m = new Sinistre(numero, pol, peritno, informeno, dataAssig, dataoBERT, dataTanc, desc, tipus, estat);
                            //Sinistres.Add(m);

                            //peritos.Add(new Perit(reader.GetInt32("NUMERO"), reader.GetString("COGNOM1"), reader.GetString("COGNOM2"), reader.GetString("NOM"), reader.GetString("NIF"), reader.GetDateTime("DATANAIX"), reader.GetString("LOGIN"), reader.GetString("PASSWORDMD5")));
                            //  _todoViewModel._allToDos.Add(new Todo(reader.GetString("whatToDO")));
                        }
                        return Trucades;
                    }
                }
            }
            catch (MySqlException)
            {
                // Handle it 
            }
            return Trucades;
        }
    }
}
