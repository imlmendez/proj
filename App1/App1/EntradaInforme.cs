﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class EntradaInforme
    {

        public EntradaInforme(int pid,int infor,DateTime data,String des,String Despres)
        {
            Num = pid;
            Informe = infor;
            Data = data;
            Desc = des;
            DespreReparacio = Despres;
        }
        private int mNUm;

        public int Num
        {
            get { return mNUm; }
            set { mNUm = value; }
        }
        private int mInforme;

        public int Informe
        {
            get { return mInforme; }
            set { mInforme = value; }
        }
        private DateTime mData;

        public DateTime Data
        {
            get { return mData; }
            set { mData = value; }
        }
        private String mDesc;

        public String Desc
        {
            get { return mDesc; }
            set { mDesc = value; }
        }

        private String mDespresReparacio;

        public String DespreReparacio
        {
            get { return mDespresReparacio; }
            set { mDespresReparacio = value; }
        }
         
       





    }
}
