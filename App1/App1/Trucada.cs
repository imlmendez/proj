﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class Trucada
    {

        public Trucada(int pid,int psinistre,DateTime hora, String des,String perso)
        {
            ID = pid;
            Sinistre = psinistre;
            Hora = mHora;
            Desc = des;
            Persona = perso;
        }
        private int mID;

        public int ID
        {
            get { return mID; }
            set { mID = value; }
        }
        private int mSinistre;

        public int Sinistre
        {
            get { return mSinistre; }
            set { mSinistre = value; }
        }

        private DateTime mHora;

        public DateTime Hora
        {
            get { return mHora; }
            set { mHora = value; }
        }
        private String mDesc;

        public String Desc
        {
            get { return mDesc; }
            set { mDesc = value; }
        }

        private String mPersona;

        public String Persona
        {
            get { return mPersona; }
            set { mPersona = value; }
        }





    }
}
