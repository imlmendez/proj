﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
   public  class Polissa
    {

        public Polissa (int pid,DateTime Datain,DateTime? Datafi,int cnum,float import, float importcontingut,float importcontinent,String tipus)
        {
            ID = pid;
            DataInici = Datain;
            DataFi = Datafi;
            ClientNum = cnum;
            Import = import;
            ImportContinent = importcontinent;
            ImportContingut = importcontingut;
            Tipus = tipus;

        }
        private int mID;

        public int ID
        {
            get { return mID; }
            set { mID = value; }
        }
        private DateTime mDataInici;

        public DateTime DataInici
        {
            get { return mDataInici; }
            set { mDataInici = value; }
        }
        private DateTime?  mDataFi;

        public DateTime?  DataFi
        {
            get { return mDataFi; }
            set { mDataFi = value; }
        }
        private int mClientNum;

        public int ClientNum
        {
            get { return mClientNum; }
            set { mClientNum = value; }
        }
        private float mImport;

        public float Import
        {
            get { return mImport; }
            set { mImport = value; }
        }
        private float mImportContinent;

        public float ImportContinent
        {
            get { return mImportContinent; }
            set { mImportContinent = value; }
        }

        private float mImporContingut;

        public float ImportContingut
        {
            get { return mImporContingut; }
            set { mImporContingut = value; }
        }

        private String mTipus;

        public String Tipus
        {
            get { return mTipus; }
            set { mTipus = value; }
        }






    }
}
