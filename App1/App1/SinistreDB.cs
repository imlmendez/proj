﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class SinistreDB
    {

        public static Boolean ModificarSinistre(Sinistre s)
        {


            string server = "127.0.0.1";
            string database = "prueba";
            string user = "root";
            string pswd = "informatica";

            string connectionString = "Server = " + server + ";database = " + database + ";uid = " + user + ";password = " + pswd + ";" + "SslMode = None;" + "charset = utf8";
            System.Text.EncodingProvider ppp;
            ppp = System.Text.CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(ppp);
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    MySqlCommand getCommand = connection.CreateCommand();
                    getCommand.CommandText = "UPDATE  SINISTRE SET PERIT_NO = @Val1,ESTATSINISTRE = @Val2 WHERE NUMERO = @Val8";
                    getCommand.Parameters.AddWithValue("@Val1", s.PeritNo);
                    getCommand.Parameters.AddWithValue("@Val2", s.Estat);
                    getCommand.Parameters.AddWithValue("@Val8", s.ID);

                    getCommand.ExecuteNonQuery();
                    return true;

                }
            }
            catch (MySqlException)
            {
                return false;
            }

        }
        public static ObservableCollection<Sinistre> GetSinistres()
        {
            ObservableCollection<Sinistre> Sinistres = new ObservableCollection<Sinistre>();
            string server = "127.0.0.1";
            string database = "prueba";
            string user = "root";
            string pswd = "informatica";

            string connectionString = "Server = " + server + ";database = " + database + ";uid = " + user + ";password = " + pswd + ";" + "SslMode = None;" + "charset = utf8";
            System.Text.EncodingProvider ppp;
            ppp = System.Text.CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(ppp);
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    MySqlCommand getCommand = connection.CreateCommand();
                    getCommand.CommandText = "SELECT NUMERO,PERIT_NO,INFORME_NO,DATAASSIGNACIO,DATAOBERTURA,DATATANCAMENT,DESCRIPCIOSINISTRE,TIPUSSINISTRE,ESTATSINISTRE,POLISSA_ID FROM SINISTRE";
                    using (MySqlDataReader reader = getCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int numero = reader.GetInt32("NUMERO");
                            int peritno = reader.GetInt32("PERIT_NO");
                            int informeno = 0;
                            int polissa = reader.GetInt32("POLISSA_ID");
                            if (!reader.IsDBNull(2))
                            {
                                informeno = reader.GetInt32("INFORME_NO");
                            }
                            DateTime? dataAssig = null;
                            if (!reader.IsDBNull(3))
                            {
                                dataAssig = reader.GetDateTime("DATAASSIGNACIO");
                            }
                            DateTime? dataoBERT = null;
                            if (!reader.IsDBNull(4))
                            {
                                dataoBERT = reader.GetDateTime("DATAOBERTURA");
                            }
                            DateTime? dataTanc = null;
                            if (!reader.IsDBNull(5))
                            {
                                dataTanc = reader.GetDateTime("DATATANCAMENT");
                            }

                            String desc = reader.GetString("DESCRIPCIOSINISTRE");
                            String tipus = reader.GetString("TIPUSSINISTRE");
                            String estat = reader.GetString("ESTATSINISTRE");

                            Sinistre m = new Sinistre(numero,polissa,peritno,informeno, dataAssig, dataoBERT, dataTanc,desc,tipus,estat);
                            Sinistres.Add(m);

                            //peritos.Add(new Perit(reader.GetInt32("NUMERO"), reader.GetString("COGNOM1"), reader.GetString("COGNOM2"), reader.GetString("NOM"), reader.GetString("NIF"), reader.GetDateTime("DATANAIX"), reader.GetString("LOGIN"), reader.GetString("PASSWORDMD5")));
                            //  _todoViewModel._allToDos.Add(new Todo(reader.GetString("whatToDO")));
                        }
                        return Sinistres;
                    }
                }
            }
            catch (MySqlException)
            {
                // Handle it 
            }
            return Sinistres;
        }

        public static ObservableCollection<Sinistre> GetSinistresFiltro(int codiclient,String Estat,int perit,DateTime? data)
        {
            ObservableCollection<Sinistre> Sinistres = new ObservableCollection<Sinistre>();
            string server = "127.0.0.1";
            string database = "prueba";
            string user = "root";
            string pswd = "informatica";

            string connectionString = "Server = " + server + ";database = " + database + ";uid = " + user + ";password = " + pswd + ";" + "SslMode = None;" + "charset = utf8";
            System.Text.EncodingProvider ppp;
            ppp = System.Text.CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(ppp);
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    MySqlCommand getCommand = connection.CreateCommand();

                    getCommand.CommandText = "SELECT NUMERO,PERIT_NO,INFORME_NO,DATAASSIGNACIO,DATAOBERTURA,DATATANCAMENT,DESCRIPCIOSINISTRE,TIPUSSINISTRE,ESTATSINISTRE,POLISSA_ID FROM SINISTRE WHERE 1 = 1 ";

                  //  ' (  (@Numero=0) or NUMERO=@Numero ) and   '
                    if(codiclient != 0)
                    {
                        getCommand.CommandText = getCommand.CommandText + " AND NUMERO = @Numero ";
                        getCommand.Parameters.AddWithValue("@Numero", codiclient);
                    }
                    if (perit != 0)
                    {
                        getCommand.CommandText = getCommand.CommandText + " AND PERIT_NO = @Perit ";
                        getCommand.Parameters.AddWithValue("@Perit", perit);
                    }
                    if (!Estat.Equals(""))
                    {
                        getCommand.CommandText = getCommand.CommandText + " AND ESTATSINISTRE LIKE @Estat ";
                        getCommand.Parameters.AddWithValue("@Estat", Estat);
                    }
                    if (data!=null)
                    {
                        getCommand.CommandText = getCommand.CommandText + " AND  DATAASSIGNACIO > @DATA ";
                        string datamysql = data?.ToString("yyyy/MM/dd",
                              CultureInfo.InvariantCulture);
                        getCommand.Parameters.AddWithValue("@DATA", datamysql);
                    }
                    getCommand.CommandText = getCommand.CommandText + ";";
                    using (MySqlDataReader reader = getCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int numero = reader.GetInt32("NUMERO");
                            int peritno = reader.GetInt32("PERIT_NO");
                            int informeno = 0;
                            if (!reader.IsDBNull(2))
                            {
                                informeno = reader.GetInt32("INFORME_NO");
                            }
                            DateTime? dataAssig = null;
                            if (!reader.IsDBNull(3))
                            {
                                dataAssig = reader.GetDateTime("DATAASSIGNACIO");
                            }
                            DateTime? dataoBERT = null;
                            if (!reader.IsDBNull(4))
                            {
                                dataoBERT = reader.GetDateTime("DATAOBERTURA");
                            }
                            DateTime? dataTanc = null;
                            if (!reader.IsDBNull(5))
                            {
                                dataTanc = reader.GetDateTime("DATATANCAMENT");
                            }

                            String desc = reader.GetString("DESCRIPCIOSINISTRE");
                            String tipus = reader.GetString("TIPUSSINISTRE");
                            String estat = reader.GetString("ESTATSINISTRE");
                            int pol = reader.GetInt32("POLISSA_ID");
                            Sinistre m = new Sinistre(numero,pol, peritno, informeno, dataAssig, dataoBERT, dataTanc, desc, tipus, estat);
                            Sinistres.Add(m);

                            //peritos.Add(new Perit(reader.GetInt32("NUMERO"), reader.GetString("COGNOM1"), reader.GetString("COGNOM2"), reader.GetString("NOM"), reader.GetString("NIF"), reader.GetDateTime("DATANAIX"), reader.GetString("LOGIN"), reader.GetString("PASSWORDMD5")));
                            //  _todoViewModel._allToDos.Add(new Todo(reader.GetString("whatToDO")));
                        }
                        return Sinistres;
                    }
                }
            }
            catch (MySqlException)
            {
                // Handle it 
            }
            return Sinistres;
        }

        public static ObservableCollection<Sinistre> GetSinistre(int codi)
        {
            ObservableCollection<Sinistre> Sinistres = new ObservableCollection<Sinistre>();
            string server = "127.0.0.1";
            string database = "prueba";
            string user = "root";
            string pswd = "informatica";

            string connectionString = "Server = " + server + ";database = " + database + ";uid = " + user + ";password = " + pswd + ";" + "SslMode = None;" + "charset = utf8";
            System.Text.EncodingProvider ppp;
            ppp = System.Text.CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(ppp);
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    MySqlCommand getCommand = connection.CreateCommand();
                    getCommand.CommandText = "SELECT NUMERO,PERIT_NO,INFORME_NO,DATAASSIGNACIO,DATAOBERTURA,DATATANCAMENT,DESCRIPCIOSINISTRE,TIPUSSINISTRE,ESTATSINISTRE,POLISSA_ID FROM SINISTRE WHERE NUMERO = @Val9";
                    getCommand.Parameters.AddWithValue("@Val9", codi);
                    using (MySqlDataReader reader = getCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int numero = reader.GetInt32("NUMERO");
                            int peritno = reader.GetInt32("PERIT_NO");
                            int informeno = 0;
                            if (!reader.IsDBNull(2))
                            {
                                informeno = reader.GetInt32("INFORME_NO");
                            }
                            DateTime? dataAssig = null;
                            if (!reader.IsDBNull(3))
                            {
                                dataAssig = reader.GetDateTime("DATAASSIGNACIO");
                            }
                            DateTime? dataoBERT = null;
                            if (!reader.IsDBNull(4))
                            {
                                dataoBERT = reader.GetDateTime("DATAOBERTURA");
                            }
                            DateTime? dataTanc = null;
                            if (!reader.IsDBNull(5))
                            {
                                dataTanc = reader.GetDateTime("DATATANCAMENT");
                            }

                            String desc = reader.GetString("DESCRIPCIOSINISTRE");
                            String tipus = reader.GetString("TIPUSSINISTRE");
                            String estat = reader.GetString("ESTATSINISTRE");
                            int pol = reader.GetInt32("POLISSA_ID");
                            Sinistre m = new Sinistre(numero,pol, peritno, informeno, dataAssig, dataoBERT, dataTanc, desc, tipus, estat);
                            Sinistres.Add(m);

                            //peritos.Add(new Perit(reader.GetInt32("NUMERO"), reader.GetString("COGNOM1"), reader.GetString("COGNOM2"), reader.GetString("NOM"), reader.GetString("NIF"), reader.GetDateTime("DATANAIX"), reader.GetString("LOGIN"), reader.GetString("PASSWORDMD5")));
                            //  _todoViewModel._allToDos.Add(new Todo(reader.GetString("whatToDO")));
                        }
                        return Sinistres;
                    }
                }
            }
            catch (MySqlException)
            {
                // Handle it 
            }
            return Sinistres;
        }
    }
}
