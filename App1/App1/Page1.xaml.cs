﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// La plantilla de elemento Página en blanco está documentada en http://go.microsoft.com/fwlink/?LinkId=234238

namespace App1
{
    /// <summary>
    /// Una página vacía que se puede usar de forma independiente o a la que se puede navegar dentro de un objeto Frame.
    /// </summary>
    public sealed partial class Page1 : Page
    {
        public static bool afegir = true;
        public static int ultimomodificado = 0;
        ObservableCollection<Perit> Perits = new ObservableCollection<Perit>();
        public Page1()
        {
            this.InitializeComponent();
            Perits = PeritDB.GetPerits();
            list1.ItemsSource = Perits;
            list2.ItemsSource = SinistreDB.GetSinistres();
        }

        private void listView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (list1.SelectedItem!=null)
            {
                Perit s = (Perit)list1.SelectedItem;
                list2.ItemsSource = SinistreDB.GetSinistre(s.ID);
            }
            
        }

        private void Accion1(object sender, RoutedEventArgs e)
        {
            btnModificar.IsEnabled = true;
            afegir = false;
            btnAfegir.IsEnabled = false;

            Button b = (Button)sender;
            int l = (int)b.Tag;
            ultimomodificado = l;


            Perit p = PeritDB.GetPerit(l);

            txbCognom.Text = p.Cognom1;
            txbCognom2.Text = p.Cognom2;
            txbNom.Text = p.Nom;
           
            txbLogin.Text = p.Login;
            txbNif.Text = p.Nif;
            txbPass.Text = p.Password;
           

            txbdata.Text  = p.DataNaixament.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);

           

            //this.Frame.Navigate(typeof(Page2), l);

        }
        private void Accion2(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            int l = (int)b.Tag;
            PeritDB.EliminarPerit(l);
            list1.ItemsSource = PeritDB.GetPerits();
            /*
            Button b = (Button)sender;
            Int64 l = (Int64)b.Tag;
            FotoDB.deleteFoto(l);
            ColorDB.deleteColor(l);
            ProducteBD.deleteProd(l);

            Productes = ProducteBD.getProductes();

            list1.ItemsSource = Productes;
            */
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            txbCognom.Text = "";
            txbCognom2.Text = "";
            txbNom.Text = "";
            txbdata.Text = "";
            txbLogin.Text = "";
            txbNif.Text = "";
            txbPass.Text = "";
            btnAfegir.IsEnabled = true;
            btnModificar.IsEnabled = false;
            afegir = true;

        }
        private Boolean comprova()
        {
            if (!txbCognom.Text.Equals("") && !txbCognom2.Text.Equals("") && !txbNom.Text.Equals("")  && !txbNif.Text.Equals("") && !txbPass.Text.Equals("") && comprovadata(txbdata.Text))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private Boolean comprovadata(String data)
        {
            string format = "dd/MM/yyyy";
            DateTime dateTime;
            if (DateTime.TryParseExact(data, format, CultureInfo.InvariantCulture,
                DateTimeStyles.None, out dateTime))
            {
                return true;
            }
            else
            {

                return false;
            }

        }

        private void btnAfegir_Click(object sender, RoutedEventArgs e)
        {
            if (comprova()) { 
                DateTime dt = DateTime.ParseExact(txbdata.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                Perit p = new Perit(0, txbCognom.Text, txbCognom2.Text, txbNom.Text, txbNif.Text, dt, txbLogin.Text, txbPass.Text);
                Boolean b =PeritDB.InsertarPerit(p);
                if (b)
                {
                    txbCognom.Text = "";
                    txbCognom2.Text = "";
                    txbNom.Text = "";
                    txbdata.Text = "";
                    txbLogin.Text = "";
                    txbNif.Text = "";
                    txbPass.Text = "";
                    list1.ItemsSource = PeritDB.GetPerits();
                }
               
               

            }
            else
            {

            }
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {

            DateTime dt = DateTime.ParseExact(txbdata.Text, "dd/M/yyyy", CultureInfo.InvariantCulture);
            Perit p = new Perit(ultimomodificado, txbCognom.Text, txbCognom2.Text, txbNom.Text, txbNif.Text, dt, txbLogin.Text, txbPass.Text);
            Boolean bb = PeritDB.ModificarPerit(p);
            if (bb)
            {
                txbCognom.Text = "";
                txbCognom2.Text = "";
                txbNom.Text = "";
                txbdata.Text = "";
                txbLogin.Text = "";
                txbNif.Text = "";
                txbPass.Text = "";
                list1.ItemsSource = PeritDB.GetPerits();
                btnModificar.IsEnabled = false;

            }
        }

        private void list2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (list2.SelectedItem != null)
            {
                txbImport.Text = "";
                txbDataInici.Text = "";
                txbdataFi.Text = "";

                Sinistre s = (Sinistre)list2.SelectedItem;
                Polissa p = PolissaDB.GetPolissa(s.Polissa);

                txbImport.Text = p.Import.ToString(); //= p.DataNaixament.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                txbDataInici.Text = p.DataInici.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                txbdataFi.Text = p.DataFi?.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                list3.ItemsSource = TrucadaDB.GetTrucades(s.ID);
                if (!s.Informe.Equals("")) {
                    list4.ItemsSource = EntradaInformeDB.GetEntrada(s.Informe);
                        }
            }
               
            
        }

        private void list3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
