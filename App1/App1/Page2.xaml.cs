﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// La plantilla de elemento Página en blanco está documentada en http://go.microsoft.com/fwlink/?LinkId=234238

namespace App1
{
    /// <summary>
    /// Una página vacía que se puede usar de forma independiente o a la que se puede navegar dentro de un objeto Frame.
    /// </summary>
    public sealed partial class Page2 : Page
    {
        public int ultimperit = 0;
        public Sinistre ultimSinistre = null;
        public Page2()
        {
            this.InitializeComponent();
            list2.ItemsSource = SinistreDB.GetSinistres();
            string[] Estat = new string[] { "NOU", "ASSIGNAT", "TANCAT"};
            comboBox.ItemsSource = Estat;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {

            int i = 0; ;
            bool successClient = int.TryParse(txbClient.Text, out i);
            int x = 0; ;
            bool successPerit = int.TryParse(txbperit.Text, out x);
           
              //    DateTime? data =  FechaSinistre.Date;

            DateTimeOffset? offset = FechaSinistre.Date;
            DateTime? dateTime=null;
            if (offset.HasValue)
            {
                dateTime = offset.Value.DateTime;
            }
           // DateTime? dateTime = offset.HasValue ? offset.Value.DateTime : null ;
            //DateTime? dateTime = offset.HasValue ? offset.Value.DateTime : null;
            string myString = "";
            if (comboBox.SelectedIndex != -1)
            {
                 myString = (String)comboBox.SelectedValue;
            }

            list2.ItemsSource = SinistreDB.GetSinistresFiltro(i, myString,x,dateTime);

        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            list2.ItemsSource = SinistreDB.GetSinistres();
        }

        private void list2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (list2.SelectedItem != null)
            {
                txbImport.Text = "";
                txbDataInici.Text = "";
                txbdataFi.Text = "";

                Sinistre s = (Sinistre)list2.SelectedItem;
                Polissa p = PolissaDB.GetPolissa(s.Polissa);

                txbImport.Text = p.Import.ToString(); //= p.DataNaixament.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                txbDataInici.Text = p.DataInici.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                txbdataFi.Text = p.DataFi?.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
               
                if (!s.Informe.Equals(""))
                {
                    list4.ItemsSource = EntradaInformeDB.GetEntrada(s.Informe);
                }
                list3.ItemsSource = TrucadaDB.GetTrucades(s.ID);
                txbperitmodifica.Text = s.PeritNo.ToString();
                ultimperit = s.PeritNo;
                ultimSinistre = s;
                if (s.DataObertura == null)
                {
                    txbperitmodifica.IsEnabled = true;
                    button3.IsEnabled = true;
                }
                else
                {
                    txbperitmodifica.IsEnabled = false;
                    button3.IsEnabled = false;
                }
            }


        }

        private void list4_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


        }

        private void btnTancar_Click(object sender, RoutedEventArgs e)
        {
            if (list2.SelectedItem != null)
            {
                Sinistre s = (Sinistre)list2.SelectedItem;
                s.Estat = "TANCAT";
                SinistreDB.ModificarSinistre(s);
                list2.ItemsSource = SinistreDB.GetSinistres();
            }

            }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            int y = 0;

            Int32.TryParse(txbperitmodifica.Text, out y);

            int x = PeritDB.ExisteixrPerit(y);
            if (x != 0 && ultimSinistre!=null)
            {

                ultimSinistre.PeritNo = y;
                SinistreDB.ModificarSinistre(ultimSinistre);
                list2.ItemsSource = SinistreDB.GetSinistres();
            }
        }
    }
}
