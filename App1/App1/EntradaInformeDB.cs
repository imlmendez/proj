﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class EntradaInformeDB
    {
        public static ObservableCollection<EntradaInforme> GetEntrada(int codi)
        {
            ObservableCollection<EntradaInforme> Entradas = new ObservableCollection<EntradaInforme>();
            string server = "127.0.0.1";
            string database = "prueba";
            string user = "root";
            string pswd = "informatica";

            string connectionString = "Server = " + server + ";database = " + database + ";uid = " + user + ";password = " + pswd + ";" + "SslMode = None;" + "charset = utf8";
            System.Text.EncodingProvider ppp;
            ppp = System.Text.CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(ppp);
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    MySqlCommand getCommand = connection.CreateCommand();
                    getCommand.CommandText = "SELECT FOTO,NUMERO,INFORME_NO,DATAENTRADA,DESCRIPCIO,DESPRESREPARACIO FROM ENTRADAINFORME WHERE INFORME_NO = @Val9";
                    getCommand.Parameters.AddWithValue("@Val9", codi);
                    using (MySqlDataReader reader = getCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {

                            int numero = reader.GetInt32("NUMERO");
                            int informe = reader.GetInt32("INFORME_NO");
                            DateTime data = reader.GetDateTime("DATAENTRADA");
                            String des = reader.GetString("DESCRIPCIO");
                            int despresreparaico = reader.GetInt32("DESPRESREPARACIO");
                            byte[] misbytes = null;
                            if (!reader.IsDBNull(0))
                            {
                                reader.GetBytes(0, 0, misbytes, 0, 10000);

                            }
                            String reparacio = null;
                            if (despresreparaico == 0)
                            {
                                reparacio = "no";
                            }
                            else
                            {
                                reparacio = "yes";
                            }

                            EntradaInforme i = new EntradaInforme(numero, informe, data, des, reparacio);
                            Entradas.Add(i);
                        }
                        return Entradas;
                    }
                }
            }
            catch (MySqlException)
            {
                // Handle it 
            }
            return Entradas;
        }
    }
}
