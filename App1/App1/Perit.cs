﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class Perit
    {


        public Perit (int pid,String cog1,String cog2,String pnom,String pnif,DateTime datanaix,String logi, String pass)
        {
            ID = pid;
            Cognom1 = cog1;
            Cognom2 = cog2;
            Nom = pnom;
            Nif = pnif;
            DataNaixament = datanaix;
            Login = logi;
            Password = pass;
        }
        private int mID;

        public int ID
        {
            get { return mID; }
            set { mID = value; }
        }

        private String mCognom1;

        public String Cognom1
        {
            get { return mCognom1; }
            set { mCognom1 = value; }
        }

        private String mCognom2;

        public String Cognom2
        {
            get { return mCognom2; }
            set { mCognom2 = value; }
        }

        private String mNom;

        public String Nom
        {
            get { return mNom; }
            set { mNom = value; }
        }

        private String mNif;

        public String Nif
        {
            get { return mNif; }
            set { mNif = value; }
        }
        private DateTime mDataNaixament;

        public DateTime DataNaixament
        {
            get { return mDataNaixament; }
            set { mDataNaixament = value; }
        }

        private String mLogin;

        public String Login
        {
            get { return mLogin; }
            set { mLogin = value; }
        }
        private String mPassword;

        public String Password
        {
            get { return mPassword; }
            set { mPassword = value; }
        }


    }
}
